<?php

/**
 * @package    Keyword_Digger
 */
 
return array(
    'debug' => true,
    'plugin_name' => 'keyword-digger',
    'version' => '1.0.0',
    'api_url' => 'http://localhost:3000/'
);