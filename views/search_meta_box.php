<input type="text" autocomplete="off" placeholder="Keyword..." name="kd_wd_keyword_input" id="kd_wd_keyword_input" require>

<select id="kd_wd_database" selected-value="<?php echo get_option('keyword_digger-database', 'us');?>">
    <option value="us">us</option>
    <option value="uk">uk</option>
    <option value="ca">ca</option>
    <option value="ru">ru</option>
    <option value="de">de</option>
    <option value="de">de</option>
    <option value="es">es</option>
    <option value="it">it</option>
    <option value="br">br</option>
    <option value="au">au</option>
    <option value="bing-us">bing-us</option>
    <option value="ar">ar</option>
    <option value="be">be</option>
    <option value="ch">ch</option>
    <option value="dk">dk</option>
    <option value="fi">fi</option>
    <option value="hk">hk</option>
    <option value="ie">ie</option>
    <option value="il">il</option>
    <option value="mx">mx</option>
    <option value="nl">nl</option>
    <option value="no">no</option>
    <option value="pl">pl</option>
    <option value="se">se</option>
    <option value="sg">sg</option>
    <option value="tr">tr</option>
    <option value="mobile-us">mobile-us</option>
    <option value="jp">jp</option>
    <option value="in">in</option>
    <option value="hu">hu</option>
</select>

<input type="submit" value="Search" tabindex="3" class="button" id="kd_wd_search" name="kd_wd_search">
<input type="button" value="x" tabindex="3" class="button" id="kd_wd_close" name="kd_wd_close">


<table class="wp-list-table widefat fixed striped kd_data_table" id="kd_wd_keyword_table" cellspacing="0" style="display:none;">
    <thead></thead>
    <tbody></tbody>
</table>

<table class="wp-list-table widefat fixed striped kd_data_table" id="kd_wd_related_keyword_table" cellspacing="0" style="display:none;">
    <thead></thead>
    <tbody></tbody>
</table>

<img src="<?php echo KEYWORD_DIGGER_URL;?>images/loading.gif" id="kd_wd_loading" style="display:none;">

<div id="kd_wd_actions" style="display:none;">
    <?php if (get_option('keyword_digger-promotion', false)): ?>
        <a href="<?php echo $config['promotion_url'];?>" target="_blank" class="kd_promotion">Upgrade to See More Keywords</a> 
    <?php endif; ?>
    
    <input type="button" value="Add Tags" class="button" id="kd_wd_add_tags" />
    <input type="button" value="Watch Density" class="button" id="kd_wd_watch_density" />
</div>

<div id="kd_wd_error" style="display:none;"></div>