<div class="wrap">
	<h1 class="kd_inline-block"> KeywordDigger - <?php echo $report_type;?> </h1>
	
	<div class="kd_float-right">
	    <a href="#" id="kd_my_account_link" class="kd_inline-block"><h4>Account</h4></a> |
	    <a href="<?php echo menu_page_url('keyword-digger', false) . '&clear_cache=true';?>" class="kd_inline-block"><h4>Clear Cache</h4></a> |
	    <a href="<?php echo menu_page_url('keyword-digger', false) . '&logout=true';?>" class="kd_inline-block"><h4>Logout</h4></a>
	</div>


    <div class="kd_top-margin">
        <table class="wp-list-table widefat fixed striped datatable" cellspacing="0">
            <tbody>
                <tr>
                    <th class="manage-column column-columnname" scope="col">Title</th>
                    <td class="column-columnname"><strong><?php echo $title;?></strong></td>
                </tr>
                <tr>
                    <th class="manage-column column-columnname" scope="col">URL</th>
                    <td class="column-columnname"><a href="<?php if (strpos($url, 'http') === false) echo 'http://' . $url; else echo $url;?>" target="_blank"><?php echo $url;?></a></td>
                </tr>
                <tr>
                    <th class="manage-column column-columnname" scope="col">Database</th>
                    <td class="column-columnname">
                        <select id="kd_change_database" class="kd_right-margin" selected-value="<?php echo $database;?>">
                            <option value="us">us</option>
                            <option value="uk">uk</option>
                            <option value="ca">ca</option>
                            <option value="ru">ru</option>
                            <option value="de">de</option>
                            <option value="de">de</option>
                            <option value="es">es</option>
                            <option value="it">it</option>
                            <option value="br">br</option>
                            <option value="au">au</option>
                            <option value="bing-us">bing-us</option>
                            <option value="ar">ar</option>
                            <option value="be">be</option>
                            <option value="ch">ch</option>
                            <option value="dk">dk</option>
                            <option value="fi">fi</option>
                            <option value="hk">hk</option>
                            <option value="ie">ie</option>
                            <option value="il">il</option>
                            <option value="mx">mx</option>
                            <option value="nl">nl</option>
                            <option value="no">no</option>
                            <option value="pl">pl</option>
                            <option value="se">se</option>
                            <option value="sg">sg</option>
                            <option value="tr">tr</option>
                            <option value="mobile-us">mobile-us</option>
                            <option value="jp">jp</option>
                            <option value="in">in</option>
                            <option value="hu">hu</option>
            	        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <div class="kd_top-margin">
        <table class="wp-list-table widefat fixed striped datatable kd_data_table" id="kd_main_table" cellspacing="0">
            <thead>
                <tr>
                <?php foreach($table->header as $header): ?>
                    <th class="manage-column column-columnname" scope="col"><?php echo $header; ?></th>
                <?php endforeach; ?>
                </tr>
            </thead>
        
            <tfoot>
                <tr>
                <?php foreach($table->header as $header): ?>
                    <th class="manage-column column-columnname" scope="col"><?php echo $header; ?></th>
                <?php endforeach; ?>
                </tr>
            </tfoot>
        
            <tbody>
            <?php foreach($table->rows as $index=>$row): ?>
                
                <?php if(!is_array($row)) continue; ?>
                
                <tr class="<?php echo ($index % 2 == 0)? 'alternate': ''; ?>">
                <?php foreach($row as $key => $value): ?>
                    <td class="column-columnname"><?php echo $value; ?></td>
                <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        
        <?php if (get_option('keyword_digger-promotion', false)): ?>
            <a href="<?php echo $config['promotion_url'];?>" target="_blank" class="kd_promotion">Upgrade to See More Keywords</a> 
        <?php endif; ?>
            
    </div>
</div>


<form method="post" action="<?php echo $config['billing_url'] . 'login'; ?>" target="_blank" id="kd_login_form" class="kd_display-none">
	 <input type="hidden" name="amember_login" value="<?php echo get_option('keyword_digger-login', '');?>" />
	 <input type="hidden" name="amember_pass" value="<?php echo get_option('keyword_digger-pass', '');?>" />
	 <input type="submit" value="Submit"/>
</form>

