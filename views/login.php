<div class="kd_login_container">
    <div class="kd_login_card"></div>
    
    <div class="kd_login_card">
        <h1 class="kd_login_title">Login</h1>
        <div class="kd_login_error"><?php echo $error; ?></div>
        
        
        <form action="<?php menu_page_url('keyword-digger'); ?>" method="POST">
            <div class="kd_login_input-container">
                <input type="text" id="login" name="login" required="required" autocomplete="off" />
                <label for="login">Username</label>
                <div class="kd_login_input-bar"></div>
            </div>
            <div class="kd_login_input-container">
                <input type="password" id="pass" name="pass" required="required" autocomplete="off" />
                <label for="pass">Password</label>
                <div class="kd_login_input-bar"></div>
            </div>
            <div class="kd_login_button-container">
                <button type="submit"><span>Go</span></button>
            </div>
            <div class="kd_login_footer"><a href="<?php echo $config['billing_url'].'login';?>" target="_blank">Forgot your password?</a></div>
        </form>
    </div>
    <div class="kd_login_card alt">
        <div class="kd_login_toggle"></div>
        <h1 class="kd_login_title">Register
      <div class="kd_login_close"></div>
    </h1>
        <form id="kd_signup_form" action="<?php echo $config['billing_url'] . 'signup';?>" method="GET">
            <div class="kd_login_input-container">
                <input type="text" id="name_f" name="name_f" required="required" autocomplete="off" />
                <label for="name_f">First Name</label>
                <div class="kd_login_input-bar"></div>
            </div>
            <div class="kd_login_input-container">
                <input type="text" id="name_l" name="name_l" required="required" autocomplete="off" />
                <label for="name_l">Last Name</label>
                <div class="kd_login_input-bar"></div>
            </div>
            <div class="kd_login_input-container">
                <input type="email" id="email" name="email" required="required" autocomplete="off" />
                <label for="email">Email</label>
                <div class="kd_login_input-bar"></div>
            </div>
            <div class="kd_login_button-container">
                <button id="kd_signup_form_submit" type="submit"><span>Next</span></button>
            </div>
        </form>
    </div>
</div>
