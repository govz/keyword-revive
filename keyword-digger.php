<?php

/**
 * @link              http://www.keywordigger.com/
 * @since             1.0.0
 * @package           Keyword_Digger
 *
 * @wordpress-plugin
 * Plugin Name:       KeywordDigger
 * Plugin URI:        http://www.keywordigger.com/
 * Description:       Research keywords directly from your wordpress. 
 * Version:           1.0.0
 * Author:            Mohit Agrawal <me@mohitagrwl.com>
 * Author URI:        http://mohitagrwl.com/
 * Text Domain:       keyword-digger
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'KEYWORD_DIGGER_PATH', plugin_dir_path( __FILE__ ) );
define( 'KEYWORD_DIGGER_URL', plugin_dir_url( __FILE__ ) );

function keyword_digger_autoload( $classname ) {
    $class = str_replace( '\\', DIRECTORY_SEPARATOR, str_replace( '_', '-', strtolower($classname) ) );

    $filePath = plugin_dir_path( __FILE__ ) . 'classes' . DIRECTORY_SEPARATOR . $class . '.php';

    if(file_exists($filePath))
    {
        require_once $filePath;
    }
}

spl_autoload_register('keyword_digger_autoload');

require_once(plugin_dir_path( __FILE__ ) . 'lib' . DIRECTORY_SEPARATOR . 'class-logger.php');

if (class_exists ( 'Keyword_Digger' ))
		$GLOBALS['Keyword_Digger'] = new Keyword_Digger();

register_activation_hook( __FILE__, array( &$GLOBALS['Keyword_Digger'], 'install' ) );
register_deactivation_hook( __FILE__, array( &$GLOBALS['Keyword_Digger'], 'uninstall' ) );

$GLOBALS['Keyword_Digger']->run();