<?php

/**
 * @package    Keyword_Digger
 */
 
class Keyword_Digger {

	public $config, $loader, $logs, $api, $admin, $views;

	public function __construct() {

		$this->config = require_once KEYWORD_DIGGER_PATH . 'config.php';
		
		if ($this->config['debug'])
		{
			@error_reporting(E_ALL & ~E_NOTICE); 
			@ini_set("display_errors", 1); 
		}
		
		$this->logs = new MyLogPHP(KEYWORD_DIGGER_PATH . 'logs' . DIRECTORY_SEPARATOR . date("Y-m-d") . '.log', ',', $this->config['debug']);
		
		$this->logs->info('Loading...');
		$this->loader = new Keyword_Digger\Loader();
		$this->load();
	}

	public function install() {
		$activator = new Keyword_Digger\Activator($this);
		$activator->run();
	}
	
	public function uninstall() {
		$deactivator = new Keyword_Digger\Deactivator($this);
		$deactivator->run();
	}
	
	private function load() {
		$this->api = new Keyword_Digger\Api($this);
		$this->admin = new Keyword_Digger\Admin($this);
		$this->views = new Keyword_Digger\Views($this);
	}

	public function run() {
		$this->loader->run();
	}
	
	public function get_url($post_id = false) {
		
		if($post_id === false)
		{
			if ($this->config['debug'])
			{
				return 'moz.com';
			}
			else
			{
				$url = parse_url(site_url());
				return $url['host'];
			}
		}
		else
		{
			if ($this->config['debug'])
			{
				return 'https://moz.com/tools';
			}
			else
			{
				return get_permalink($post_id);
			}
		}
	}
}
