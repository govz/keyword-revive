<?php

/**
 * @package    Keyword_Digger
 * @subpackage Keyword_Digger\Activator
 */
 
namespace Keyword_Digger;

class Activator {
	
	private $Keyword_Digger;
	
	public function __construct($Keyword_Digger) {
		$this->Keyword_Digger= $Keyword_Digger;
	}
	
	public function run() {
		$this->Keyword_Digger->logs->info('Activating plugin....');
	}

}
