<?php

/**
 * @package    Keyword_Digger
 * @subpackage Keyword_Digger\Deactivator
 */
 
namespace Keyword_Digger; 
 
class Deactivator {
	
	private $Keyword_Digger;
	
	public function __construct($Keyword_Digger) {
		$this->Keyword_Digger= $Keyword_Digger;
	}

	public function run() {
		
		@set_time_limit(0);
		@ini_set('max_execution_time', 300); 
		
		$this->Keyword_Digger->logs->info('Deactivating plugin....');
		delete_option('keyword_digger-active');
		delete_option('keyword_digger-login'); 
		delete_option('keyword_digger-pass'); 
		delete_option('keyword_digger-setup'); 
		delete_option('keyword_digger-database'); 
		
		$this->Keyword_Digger->api->clear_cache();
	}

}
