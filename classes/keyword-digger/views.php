<?php

/**
 * @package    Keyword_Digger
 * @subpackage Keyword_Digger\Views
 */
 
namespace Keyword_Digger;

class Views {

	private $Keyword_Digger;

	public function __construct($Keyword_Digger) {
		$this->Keyword_Digger= $Keyword_Digger;
		$this->load();
	}

	private function load() {
	    
	}
	
	public function dashboard() {
		
		$config = $this->Keyword_Digger->config;
		$error = false;
		
		if ($_GET['logout'] === 'true')
		{
			update_option('keyword_digger-active', false);
		}
		
		if ($_GET['clear_cache'] === 'true')
		{
			$this->Keyword_Digger->api->clear_cache();
		}
		
		if ($_POST['login'] && $_POST['pass'])
		{
			$result = $this->Keyword_Digger->api->login($_POST['login'], $_POST['pass']);	
			
			if ($result === false)
			{
				$error = "Unable to reach server. Please try again laster.";
			}
			elseif ($result->ok === false) {
				$error = $result->msg;
			}
			else {
				$error = false;
			}
		}
		
		if (get_option('keyword_digger-active', false))
		{
			$database = $_GET['database'] ? $_GET['database']  : get_option('keyword_digger-database', 'us'); 
			
			if ($_GET['database'])
			{
				update_option('keyword_digger-database', $database); 
			}
			
			$post = $_GET['post'] ? $_GET['post']  : false ; 
			
			if ($post === false)
			{
				$report_type = 'Domain Report';
				$url = $this->Keyword_Digger->get_url();
				$title = get_bloginfo( 'name' );
				$table = $this->Keyword_Digger->api->query('domainOrganic', array('display_limit' => 1000, 'domain' => $url, 'database' => $database, 'export_columns' => 'Ph,Po,Pp,Pd,Nq,Cp,Tr,Tc,Co,Nr,Ur,Td'));
			}
			else 
			{
				$report_type = 'Post/Page Report';
				$url = $this->Keyword_Digger->get_url((int) $post);
				$title = get_the_title( (int) $post);
				$table = $this->Keyword_Digger->api->query('urlOrganic', array('display_limit' => 1000, 'url' => $url, 'database' => $database, 'export_columns' => 'Ph,Po,Nq,Cp,Co,Tr,Tc,Nr,Td'));
			}
			
			if ($table === false)
				$error = 'Unable to reach server. Please try again laster.';
			elseif (property_exists($table, 'ok') && $table->ok === false) {
				$error = $table->msg;
				
				if ($table->code === 'unauthorized')
					$error = 'unauthorized';
			}
			
			if ($error == 'unauthorized')
			{
				$error = false;
				include(KEYWORD_DIGGER_PATH . 'views/login.php');
			}
			else if ($error) {
				include(KEYWORD_DIGGER_PATH . 'views/error.php');
			}
			else 
			{
				include(KEYWORD_DIGGER_PATH . 'views/dashboard.php');
			}
		}
		else {
			include(KEYWORD_DIGGER_PATH . 'views/login.php');
		}
	}
	
	public function search_meta_box($post, $callback_args) {
		$config = $this->Keyword_Digger->config;
		
		include(KEYWORD_DIGGER_PATH . 'views/search_meta_box.php');
	}
}
