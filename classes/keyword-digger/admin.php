<?php

/**
 * @package    Keyword_Digger
 * @subpackage Keyword_Digger\Admin
 */
 
namespace Keyword_Digger;

class Admin {

	private $Keyword_Digger;

	public function __construct($Keyword_Digger) {
		$this->Keyword_Digger= $Keyword_Digger;
		$this->load();
	}

	private function load() {
		$this->Keyword_Digger->loader->add_action( 'admin_enqueue_scripts', $this, 'enqueue_styles' );
		$this->Keyword_Digger->loader->add_action( 'admin_enqueue_scripts', $this, 'enqueue_scripts' );
		
		$this->Keyword_Digger->loader->add_action( 'admin_menu', $this, 'admin_menu' );
		$this->Keyword_Digger->loader->add_filter( 'post_row_actions', $this, 'add_post_action', 10 , 2);
		$this->Keyword_Digger->loader->add_filter( 'page_row_actions', $this, 'add_post_action', 10 , 2);
		
		$this->Keyword_Digger->loader->add_filter( 'add_meta_boxes', $this, 'adding_meta_boxes', 10 , 2);
	}
	
	public function enqueue_styles() {
		wp_enqueue_style( $this->Keyword_Digger->config['plugin_name'], KEYWORD_DIGGER_URL . 'css/main.css', array(), $this->Keyword_Digger->config['version'], 'all' );
		wp_enqueue_style( 'jquery.dataTables', KEYWORD_DIGGER_URL . 'css/jquery.dataTables.min.css', array(), $this->Keyword_Digger->config['version'], 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script( $this->Keyword_Digger->config['plugin_name'], KEYWORD_DIGGER_URL . 'js/main.js', array( 'jquery' ), $this->Keyword_Digger->config['version'], false );
		wp_enqueue_script( 'jquery.dataTables', KEYWORD_DIGGER_URL . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->Keyword_Digger->config['version'], false );
	}

	public function add_post_action($actions, $post) {
		
		if (get_option('keyword_digger-active', false))
		{
			$actions['keyword_report'] = '<a target="_black" href="'. menu_page_url('keyword-digger', false) . '&post='. $post->ID .'">Keyword Report</a>';
		}
		
		return $actions;
	}
	
	public function admin_menu() {
		
		add_menu_page('KeywordDigger', 'KeywordDigger', 'edit_posts', 'keyword-digger', array($this->Keyword_Digger->views, 'dashboard'), 'dashicons-search');
	}
	
	public function adding_meta_boxes($post_type, $post) {
		
		if($post_type !== 'post' && $post_type !== 'page')
			return false;
			
	    add_meta_box('keyword_digger_search_box', __( 'Keyword Digger' ), array($this->Keyword_Digger->views, 'search_meta_box'), 'post', 'normal', 'default');
	}
	
}
