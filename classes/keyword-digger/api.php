<?php

/**
 * @package    Keyword_Digger
 * @subpackage Keyword_Digger\Api
 */
 
namespace Keyword_Digger;

class Api {

	private $Keyword_Digger;

	public function __construct($Keyword_Digger) {
		$this->Keyword_Digger= $Keyword_Digger;
		$this->load();
	}

	private function load() {
	     $this->setup();
	     $this->Keyword_Digger->loader->add_action('wp_ajax_keyword_digger', $this, 'ajax');
	}
	
	public function login($login, $pass) {
	    
	    $args = array(
            'timeout'     => 30,
            'redirection' => 5,
            'httpversion' => '1.0',
            'user-agent'  => 'WordPress; ' . get_bloginfo( 'url' ),
            'blocking'    => true,
            'headers'     => array(),
            'cookies'     => array(),
            'body'        => null,
            'compress'    => false,
            'decompress'  => true,
            'sslverify'   => true,
            'stream'      => false,
            'filename'    => null
        );

        $response = wp_remote_get($this->Keyword_Digger->config['api_url'] . 'login?' . http_build_query(array(
            'login' => $login,
            'pass' => $pass,
            'domain' => $this->Keyword_Digger->get_url()
        )), $args);
        
        if( is_array($response) ) 
        {
            $body = $response['body']; 
            
            $body = @json_decode($body);
            
            if ($body !== null)
            {
                if ($body->ok === true)
                {
                    update_option('keyword_digger-login', $login);
                    update_option('keyword_digger-pass', $pass);
                    update_option('keyword_digger-active', true);
                    update_option('keyword_digger-promotion', $body->promotion);
                }
                else
                {
                    update_option('keyword_digger-active', false);
                }

                return $body;
            }        
        }
        
        return false;
	}
	
	
	public function query($operation, $parameters = array()) {
	    
	    $parameters['login'] = get_option('keyword_digger-login', ''); 
	    $parameters['pass'] = get_option('keyword_digger-pass', '');
	    
        $url = $this->Keyword_Digger->config['api_url'] . 'query/' . $operation . '?' . http_build_query($parameters);
        $url_hash = 'kd_' . md5($url);
        
        $cache = get_transient( $url_hash );
        
        if ($cache !== false)
            return $cache;
        
        $args = array(
            'timeout'     => 86400,
            'redirection' => 5,
            'httpversion' => '1.0',
            'user-agent'  => 'WordPress; ' . get_bloginfo( 'url' ),
            'blocking'    => true,
            'headers'     => array(),
            'cookies'     => array(),
            'body'        => null,
            'compress'    => false,
            'decompress'  => true,
            'sslverify'   => true,
            'stream'      => false,
            'filename'    => null
        );
        
        $response = wp_remote_get($url, $args);
        
        $this->Keyword_Digger->logs->debug(json_encode($response));
        
        if( is_array($response) ) 
        {
            $body = $response['body']; 
            
            $body = @json_decode($body);
            
            if ($body !== null)
            {
                if ($body->ok === false && $body->code === 'unauthorized')
                {
                    update_option('keyword_digger-active', false);
                }
                
                return $body;
            }        
            
            $result = new \stdClass();
            $lines = @explode(PHP_EOL, $response['body']);
            
            $rows = array();
            foreach ($lines as $line) {
                 @array_push($rows, @str_getcsv($line, ';')); 
            }
            
            $result->header = @array_shift($rows);
            $result->slugs = @array_map(array($this, 'slugify'), $result->header);
            
            
            $csv = array();
            foreach ($rows as $row) 
            {
                $csv[] = @array_combine($result->slugs, $row);
            }
            
            $result->rows = $csv;
            
            @set_transient( $url_hash, $result, 864000 );
            
            $transients = get_option('keyword_digger-transients', array());
            array_push($transients, $url_hash);
            update_option('keyword_digger-transients', $transients, false);
            
            return $result;
        }
        
        return false;
	}
	
	public function setup() 
	{
	    $setup = get_option( 'keyword_digger-setup', false);
	    
	    if ($setup !== false)
	    {
	       foreach ($setup as $key => $config) 
	       {
	           $this->Keyword_Digger->config[$key] = $config;
	       }
	       
	       return true;
	    }
	    
	    $url = $this->Keyword_Digger->config['api_url'] . 'setup';
	    
	    $args = array(
            'timeout'     => 30,
            'redirection' => 5,
            'httpversion' => '1.0',
            'user-agent'  => 'WordPress; ' . get_bloginfo( 'url' ),
            'blocking'    => true,
            'headers'     => array(),
            'cookies'     => array(),
            'body'        => null,
            'compress'    => false,
            'decompress'  => true,
            'sslverify'   => true,
            'stream'      => false,
            'filename'    => null
        );
        
        $response = wp_remote_get($url, $args);
	    
	    if( is_array($response) ) 
        {
            $body = $response['body']; 
            
            $body = @json_decode($body, true);
            
            if ($body !== null)
            {
                $setup = update_option( 'keyword_digger-setup', $body, true);
                
                return $this->setup();
            }
        }
        
        return false;
	}
	
	public function ajax() {

	    $operation = $_POST['operation'];
	    unset($_POST['operation']);
	    unset($_POST['action']);
	    
	    $result = $this->query($operation, $_POST);
	    
	    if ($result === false)
	        echo('{"ok": false, "code": "internal_server_error", "msg": "Internal Server Error"}');
	    else 
	        echo(@json_encode($result));
	        
	    wp_die();
	}
	
	public function clear_cache()
	{
	    global $wpdb;
	    $wpdb->query("DELETE FROM `wp_options` WHERE `option_name` LIKE ('%\_transient\_kd_%') OR `option_name` LIKE ('%\_transient\_timeout\_kd_%')");
	   
	    $transients = get_option('keyword_digger-transients', array());
	    
	    foreach ($transients as $transient ) {
	        delete_transient( $transient );
	    }
	}
	
	static public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '_', $text);
        
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        
        // trim
        $text = trim($text, '_');
        
        // remove duplicate -
        $text = preg_replace('~-+~', '_', $text);
        
        // lowercase
        $text = strtolower($text);
        
        if (empty($text))
        {
            return 'n-a';
        }
        
        return $text;
    }
}
