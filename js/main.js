/**
 * @package    Keyword_Digger
 */
 
(function($) {
	'use strict';
	
    $ = jQuery;

	$(document).ready(function()
	{
	    $.fn.dataTable.ext.errMode = 'none';
	    
        $("#kd_change_database").val($('#kd_change_database').attr('selected-value') ? $('#kd_change_database').attr('selected-value') : 'us');
        $("#kd_wd_database").val($('#kd_wd_database').attr('selected-value') ? $('#kd_wd_database').attr('selected-value') : 'us');
        
        $('#kd_change_database').on('change', function() 
        {
            var query = window.location.search;
            console.log(query);
            query = query.slice(1);
            console.log(query);
            query = deparam(query);
            console.log(query);
            query.database = this.value;
            console.log(query);
            query = '?' + $.param(query);
            console.log(query);
            window.location.search = query;
        });
        
        $('#kd_my_account_link').on('click', function()
        {
            $('#kd_login_form').submit();
        });
        
        $('.kd_login_toggle').on('click', function() 
        {
            $('.kd_login_container').stop().addClass('active');
        });
        
        $('.kd_login_close').on('click', function() 
        {
            $('.kd_login_container').stop().removeClass('active');
        });

        
        $('#kd_main_table').dataTable();
        
        /************************************************/
        
        var typingTimer;                
        var doneTypingInterval = 1000; 
        var $input = $('#kd_wd_keyword_input');

        $input.on('keyup', function () 
        {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function() 
            {
                $('#kd_wd_search').click();
            }, doneTypingInterval);
        });
        
        $input.on('keydown', function () 
        {
            clearTimeout(typingTimer);
        });
        
        
        $('#kd_wd_database').on('change', function() 
        {
            $('#kd_wd_search').click();
        });
        
        $('#kd_wd_close').on('click', function(e) 
        {
            e.preventDefault();
            
            $('#kd_wd_error').hide();
            $('#kd_wd_keyword_table').hide();
            $('#kd_wd_actions').hide();
            $('#kd_wd_related_keyword_table').hide();
            
            if (related_keyword_table)
                    related_keyword_table.fnDestroy();
                    
            $('#kd_wd_keyword_table tr').remove();
            $('#kd_wd_related_keyword_table tr').remove();
        });
        
        /************************************************/
        
        var related_keyword_table;
        
        var status_main = false;
        var status_related = false;
        var error = false;
        
        var do_something = function() 
        {
            if (error)
            {
                $('#kd_wd_error').html(error);
                $('#kd_wd_error').show();
            }
            else
            {
                $('#kd_wd_error').hide();
            }
            
            if (status_main === 'loading' || status_related === 'loading')
            {
                $('#kd_wd_loading').show();
            }
            else
            {
                $('#kd_wd_loading').hide();
            }
            
            if (status_main === 'success')
            {
                $('#kd_wd_keyword_table').show();
            }
            else
            {
                $('#kd_wd_keyword_table').hide();
            }
            
            if (status_related === 'success')
            {
                $('#kd_wd_related_keyword_table').show();
            }
            else
            {
                $('#kd_wd_related_keyword_table').hide();
            }
            
            if  (status_main === 'success' || status_related === 'success')
            {
                $('#kd_wd_actions').show();
            }
            else
            {
                $('#kd_wd_actions').hide();
            }
        };
        
        $('#kd_wd_search').on('click', function(e) 
        {
            e.preventDefault();
            
            $.ajaxSetup({
                timeout: 86400000 
            });
                       
            status_main = 'loading';
            status_related = 'loading';
            error = false;
            do_something();
                
            if (related_keyword_table)
                related_keyword_table.fnDestroy();
                    
            $('#kd_wd_keyword_table tr').remove();
            $('#kd_wd_related_keyword_table tr').remove();
            
            jQuery.post(window.ajaxurl, 
            {
                action: 'keyword_digger',
                operation: 'phraseAll',
                database: $('#kd_wd_database').val(),
                phrase: $.trim($('#kd_wd_keyword_input').val()),
                export_columns: 'Ph,Nq,Cp,Co'
            }, 
            function(response) 
            {
    			try
    			{
                    var result = JSON.parse(response);
                }
                catch(e)
                {
                    console.error(e);
                    
                    status_main = 'error';
                    error = 'Error. Please try again later.';
                    return do_something();
                }
                
                if (result.msg)
                {
                    status_main = 'error';
                    error = result.msg;
                    return do_something();
                }
                
                console.log('Result', result);

                var row = $('<tr><th class="column-cb check-column"></th></tr>');
                
                for (var title in result.header)
                {
                    title = result.header[title];
                    $('<th></th>').text(title).appendTo(row); 
                }
                
                row.appendTo('#kd_wd_keyword_table thead');
                
                for (var row in result.rows)
                {
                    row = result.rows[row];
                    
                    if (typeof row !== 'object')
                        continue;
                        
                    var hrow = $('<tr><th scope="row" class="check-column"><input type="checkbox" name="kd_wd_selects[]" value="'+row['phrase']+'"></th></tr>');
                    
                    for (var slug in result.slugs)
                    {
                        slug = result.slugs[slug];
                        $('<td></td>').text(row[slug]).appendTo(hrow); 
                    }
                    
                    hrow.appendTo('#kd_wd_keyword_table tbody');
                }

                status_main = 'success';
                return do_something();
                
    		}).fail(function(error)
    		{
    		    console.error(error);

                status_main = 'error';
                error = 'Error. Please try again later.';
                return do_something();
    		});
    		
    		/************************************************/
    		
    		jQuery.post(window.ajaxurl, 
            {
                action: 'keyword_digger',
                operation: 'phraseRelated',
                database: $('#kd_wd_database').val(),
                phrase: $.trim($('#kd_wd_keyword_input').val()),
                display_limit: 500,
                export_columns: 'Ph,Nq,Cp,Co,Nr,Td'
            }, 
            function(response) 
            {
    			try
    			{
                    var result = JSON.parse(response);
                }
                catch(e)
                {
                    console.error(e);
                    
                    status_related = 'error';
                    error = 'Error. Please try again later.';
                    return do_something();
                }
                
                if (result.msg)
                {
                    status_related = 'error';
                    error = result.msg;
                    return do_something();
                }
                
                console.log('Result', result);
                
                var row = $('<tr><td class="manage-column column-cb check-column"><input type="checkbox" id="kd_wd_select_all" name="kd_wd_select_all" value="1"></td></tr>');
                
                for (var title in result.header)
                {
                    title = result.header[title];
                    $('<th></th>').text(title).appendTo(row); 
                }
                
                row.appendTo('#kd_wd_related_keyword_table thead');
                
                for (var row in result.rows)
                {
                    row = result.rows[row];
                    
                    if (typeof row !== 'object')
                        continue;
                        
                    var hrow = $('<tr><th scope="row" class="check-column"><input type="checkbox" name="kd_wd_selects[]" value="'+row['keyword']+'"></th></tr>');
                    
                    for (var slug in result.slugs)
                    {
                        slug = result.slugs[slug];
                        $('<td></td>').text(row[slug]).attr('kd_col', slug).appendTo(hrow); 
                    }
                    
                    hrow.appendTo('#kd_wd_related_keyword_table tbody');
                }

                setTimeout(function()
                {
                    related_keyword_table = $('#kd_wd_related_keyword_table').dataTable(
                    {
                        searching: false,
                        lengthChange: false,
                        pageLength: 5,
                        columnDefs: [{
                            targets: 0,
                            searchable: false,
                            orderable: false,
                        }]
                    });
                    
                    $('#kd_wd_select_all').on('click', function() 
                    {
                        $('input[name=kd_wd_selects\\[\\]]').prop('checked', this.checked);
                    });
                });
                
                status_related = 'success';
                return do_something();
                
    		}).fail(function(error)
    		{
    		    console.error(error);
    		                     
    		    status_related = 'error';
                error = 'Error. Please try again later.';
                return do_something();
    		});

        });
        
        /************************************************/
        
        $('#kd_wd_add_tags').on('click', function() 
        {
            var checkboxes = jQuery('input[name=kd_wd_selects\\[\\]]:checked');
            
            for (var box in checkboxes)
            {
                var value = $(checkboxes[box]).val();
                
                if (typeof value === 'string') 
                {
                    $("#new-tag-post_tag").val(value); 
                    $("#new-tag-post_tag").closest("input").submit();
                }
            }
        });
    
    });
    
})(jQuery);


(function(deparam){
    if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
        try {
            var jquery = require('jquery');
        } catch (e) {
        }
        module.exports = deparam(jquery);
    } else if (typeof define === 'function' && define.amd){
        define(['jquery'], function(jquery){
            return deparam(jquery);
        });
    } else {
        var global;
        try {
          global = (false || eval)('this'); // best cross-browser way to determine global for < ES5
        } catch (e) {
          global = window; // fails only if browser (https://developer.mozilla.org/en-US/docs/Web/Security/CSP/CSP_policy_directives)
        }
        global.deparam = deparam(global.jQuery); // assume jQuery is in global namespace
    }
})(function ($) {
    var deparam = function( params, coerce ) {
        var obj = {},
        coerce_types = { 'true': !0, 'false': !1, 'null': null };

        // Iterate over all name=value pairs.
        params.replace(/\+/g, ' ').split('&').forEach(function(v){
            var param = v.split( '=' ),
            key = decodeURIComponent( param[0] ),
            val,
            cur = obj,
            i = 0,

            // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
            // into its component parts.
            keys = key.split( '][' ),
            keys_last = keys.length - 1;

            // If the first keys part contains [ and the last ends with ], then []
            // are correctly balanced.
            if ( /\[/.test( keys[0] ) && /\]$/.test( keys[ keys_last ] ) ) {
                // Remove the trailing ] from the last keys part.
                keys[ keys_last ] = keys[ keys_last ].replace( /\]$/, '' );

                // Split first keys part into two parts on the [ and add them back onto
                // the beginning of the keys array.
                keys = keys.shift().split('[').concat( keys );

                keys_last = keys.length - 1;
            } else {
                // Basic 'foo' style key.
                keys_last = 0;
            }

            // Are we dealing with a name=value pair, or just a name?
            if ( param.length === 2 ) {
                val = decodeURIComponent( param[1] );

                // Coerce values.
                if ( coerce ) {
                    val = val && !isNaN(val) && ((+val + '') === val) ? +val        // number
                    : val === 'undefined'                       ? undefined         // undefined
                    : coerce_types[val] !== undefined           ? coerce_types[val] // true, false, null
                    : val;                                                          // string
                }

                if ( keys_last ) {
                    // Complex key, build deep object structure based on a few rules:
                    // * The 'cur' pointer starts at the object top-level.
                    // * [] = array push (n is set to array length), [n] = array if n is
                    //   numeric, otherwise object.
                    // * If at the last keys part, set the value.
                    // * For each keys part, if the current level is undefined create an
                    //   object or array based on the type of the next keys part.
                    // * Move the 'cur' pointer to the next level.
                    // * Rinse & repeat.
                    for ( ; i <= keys_last; i++ ) {
                        key = keys[i] === '' ? cur.length : keys[i];
                        cur = cur[key] = i < keys_last
                        ? cur[key] || ( keys[i+1] && isNaN( keys[i+1] ) ? {} : [] )
                        : val;
                    }

                } else {
                    // Simple key, even simpler rules, since only scalars and shallow
                    // arrays are allowed.

                    if ( Object.prototype.toString.call( obj[key] ) === '[object Array]' ) {
                        // val is already an array, so push on the next value.
                        obj[key].push( val );

                    } else if ( {}.hasOwnProperty.call(obj, key) ) {
                        // val isn't an array, but since a second value has been specified,
                        // convert val into an array.
                        obj[key] = [ obj[key], val ];

                    } else {
                        // val is a scalar.
                        obj[key] = val;
                    }
                }

            } else if ( key ) {
                // No value was defined, so set something meaningful.
                obj[key] = coerce
                ? undefined
                : '';
            }
        });

        return obj;
    };
    if ($) {
      $.prototype.deparam = $.deparam = deparam;
    }
    return deparam;
});